import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    CountryService,
    EventService,
    LoadingService,
    NodeService,
    ApiBase,
    BreadcrumbService,
    CarService,
    ErrorHandlerService
 } from './service.index';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    CountryService,
    EventService,
    LoadingService,
    NodeService,
    ApiBase,
    BreadcrumbService,
    CarService,
    ErrorHandlerService
  ],
  declarations: []
})
export class ServiceModule { }
