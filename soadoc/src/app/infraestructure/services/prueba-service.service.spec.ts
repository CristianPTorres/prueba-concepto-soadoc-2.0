import { TestBed } from '@angular/core/testing';

import { PruebaServiceService } from './prueba-service.service';

describe('PruebaServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PruebaServiceService = TestBed.get(PruebaServiceService);
    expect(service).toBeTruthy();
  });
});
