import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Cliente } from 'src/app/domain/clienteDTO';
import {ApiBase} from 'src/app/infraestructure/services/api/api-base.service';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PruebaServiceService {

  constructor( private apiBase: ApiBase ) {}

  getListaPersonas(): Observable<any> {
    return this.apiBase.list(environment.prueba_concepto).pipe(map( res => res.body));
  }

  postCrearPersona(persona: Cliente): Observable<any> {
    return this.apiBase.post(environment.prueba_concepto, persona);

  }

  putEditarPersona(id, persona: Cliente ): Observable<any> {
    return this.apiBase.put(`${environment.prueba_concepto}${id}`, persona);
  }

  deletePersona(id) {
        return this.apiBase.delete(`${environment.prueba_concepto}${id}`, {});
    }

}

