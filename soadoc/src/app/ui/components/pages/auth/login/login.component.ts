import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import { State as RootState} from '../../../../../infraestructure/redux_store/reducers';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private _store: Store<RootState>) { }

  ngOnInit() {
  }

}
