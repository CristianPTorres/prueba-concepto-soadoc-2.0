import {Component, OnDestroy, OnInit} from '@angular/core';
import {Validators, FormControl, FormGroup, FormBuilder} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import {  Message, SelectItem } from 'primeng/primeng';
import { Cliente } from 'src/app/domain/clienteDTO';
import { PruebaServiceService } from 'src/app/infraestructure/services/prueba-service.service';
import {Observable, Subscription} from 'rxjs';



@Component({
    selector: 'app-prueba',
    templateUrl: './prueba.component.html',
    styleUrls: ['./prueba.component.css']
})
export class PruebaComponent implements OnInit, OnDestroy {

    formularioCrear: FormGroup;
    formularioModificar: FormGroup;
    persona: Cliente[] = [];
    listaPersonas$;
    seleccionado: any[] = [];
    cols: any[];
    showDialog: boolean = false;
    oculto: boolean = true;
    mostrartabla: boolean = false;
    msgs: Message[] = [];
    idSeleccion: any;
    documento: SelectItem[];
    subscription = new Subscription();



    constructor(private http: HttpClient,
                private pruebaService: PruebaServiceService,
                private fe: FormBuilder
    ) {}
    ngOnInit() {
        this.initForm();

        this.cols = [
            { field: 'nombres', header: 'Nombre' },
            { field: 'apellidos', header: 'Apellido' },
            { field: 'tipoIdentificacion', header: 'Tipo Id' },
            { field: 'numeroIdentificacion', header: 'Numero Id' }
        ];
    }

    initForm(): void {
        this.documento = [
            {label: 'Cedula', value: 'CC'},
            {label: 'Tarjeta de identidad', value: 'TI'},
            {label: 'Cedula de extrangería', value: 'CE'},
        ];

        this.formularioCrear = this.fe.group({
            nombres: [null, Validators.required],
            apellidos: [null, Validators.required],
            tipoIdentificacion: [null, Validators.required],
            numeroIdentificacion: [null, Validators.required],
        });

        this.formularioModificar = new FormGroup({
            'Nombre': new FormControl('', Validators.required),
            'Apellido': new FormControl('', Validators.required),
            'TipDoc': new FormControl('', Validators.required),
            'NumDoc': new FormControl('', Validators.required),
            'Id': new FormControl(''),
        });
    }


    crearPersonas() {

       const payload = {
            id: this.formularioCrear.value.id,
            nombres: this.formularioCrear.value.nombres,
            apellidos: this.formularioCrear.value.apellidos,
            tipoIdentificacion: this.formularioCrear.value.tipoIdentificacion,
            numeroIdentificacion: this.formularioCrear.value.numeroIdentificacion
        };

        this.pruebaService.postCrearPersona(payload).subscribe()
        this.listarPersonas()
        this.formularioCrear.reset();

    }

    listarPersonas() {
        this.mostrartabla = !this.mostrartabla;
        this.subscription = this.pruebaService.getListaPersonas()
            .subscribe(persona => {
                this.persona = persona.body;
            });
    }



    modificarPersona(persona: any[]) {
        this.seleccionado = persona;
        this.showDialog = true;
        return this.seleccionado;
    }


    guardarModificacion(id: any) {
        const payload: Cliente = {
            id: this.formularioModificar.value.id,
            nombres: this.formularioModificar.value.Nombre,
            apellidos: this.formularioModificar.value.Apellido,
            tipoIdentificacion: this.formularioModificar.value.TipDoc,
            numeroIdentificacion: this.formularioModificar.value.NumDoc,
        };
       this.pruebaService.putEditarPersona( id , payload ).subscribe();
       this.listarPersonas();
        this.formularioModificar.reset();
        this.hideDialog();
    }
    eliminarPersona(id) {
        this.pruebaService.deletePersona(id).subscribe();
        this.listarPersonas();

    }

    hideDialog() {
        this.showDialog = false;
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}



