import { PAGES_ROUTES_MODULE } from './pages-routing.module';
import { PRIMENG_MODULES } from './../../shared/primeNg/primeng-elements';
import { PagesComponent } from './pages.component';
import { LayoutModule } from './layouts/layout.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './pages/auth/login/login.component';
import { RegisterComponent } from './pages/auth/register/register.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { PruebaComponent } from './pages/prueba/prueba.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
      PagesComponent,
      LoginComponent,
      RegisterComponent,
      PageNotFoundComponent,
      PruebaComponent
  ],
  exports: [
      PagesComponent,
      PruebaComponent,
  
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    ...PRIMENG_MODULES,
    PAGES_ROUTES_MODULE
  ]
})
export class PagesModule { }
