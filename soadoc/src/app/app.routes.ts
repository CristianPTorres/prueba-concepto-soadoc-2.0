import { PagesComponent } from './ui/components/pages.component';
import { LoginComponent } from './ui/components/pages/auth/login/login.component';
import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { PageNotFoundComponent } from './ui/components/pages/page-not-found/page-not-found.component';

export const APP_ROUTES: Routes = [
    {path: 'login', component: LoginComponent},
    {
        path: '',
        component: PagesComponent,
        loadChildren: './ui/components/pages.module#PagesModule'
    },
    {path: '**', component: PageNotFoundComponent},
];

export const APP_ROUTES_MODULE: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES, {scrollPositionRestoration: 'enabled'});
